
<?php
/**
 * Plugin Name: Cat Facts
 * Plugin URI: https://gitlab.com/technical-test-hellosafe/
 * Description: A plugin that displays cat facts.
 * Text-Domain: my-cat-facts
 * Version: 1.0.0
 * Author: Fanilo RAVELOSON
 * Author URI: https://gitlab.com/technical-test-hellosafe/
 * License: GPL2
 *
 * @package My_WordPress_Plugin
 */

if(!defined('ABSPATH')) exit; // Exit if accessed directly

if(!class_exists('CatFacts')) {
    class CatFacts {

        public function __construct() {
            
        }

        public function initialize() {
            add_shortcode('cat-facts', 'cat_facts_shortcode');

            function cat_facts_shortcode($atts) {
                $height = isset($atts['height']) ? $atts['height'] : '1000px';
                return '<div><iframe src="http://localhost:3000" frameborder="0" style="width:100%; height:' . $height . '"></iframe></div>';
            }
        
            add_action('admin_menu', 'create_admin_menu');

            function create_admin_menu() {
                add_menu_page(
                    'Cat Facts',
                    'Cat Facts',
                    'manage_options',
                    'cat-facts',
                    'cat_facts_page',
                    'dashicons-pets',
                    6
                );
            }

            function cat_facts_page() {
                echo 'Nothing to see here.';
            }
        }

        
    }

    $cat_facts = new CatFacts();
    $cat_facts->initialize();
}
?>
